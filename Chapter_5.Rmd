---
title: "R for Data Science: Notes & Exercises from Chapter 5"
output: html_notebook
---

```{r}
library(nycflights13)
library(tidyverse)

flights
```



## 5.2.4 Exercises

### 1. Find all flights that:

#### Had an arrival delay of two or more hours

```{r}
f.d.2 <- filter(flights, arr_delay >= 2)

f.d.2
```

```{r}
summary(f.d.2$arr_delay)
```


#### Flew to Houston (IAH or HOU)


```{r}
f.to.houston.1 <- filter(flights, dest %in% c('IAH', 'HOU'))

f.to.houston.1
```

```{r}
unique(f.to.houston.1$dest)
```

longer but still correct
```{r}
f.to.houston.2 <- filter(flights, dest == 'IAH' | dest == 'HOU')

identical(f.to.houston.1, f.to.houston.2)
```


#### Were operated by United, American, or Delta

reading at `?flights` I found:

```{r}
airlines
```

```{r}
carriers.3 <- filter(flights, carrier %in% c('UA','AA','DL'))

carriers.3
```

```{r}
unique(carriers.3$carrier)
```

#### Departed in summer (July, August, and September)

```{r}
dep.summer <- filter(flights, month %in% c(7,8,9))

dep.summer
```

```{r}
unique(dep.summer$month)
```


#### Arrived more than two hours late, but didn’t leave late

```{r}
lg2.dot <- filter(flights, arr_delay > 2 & dep_delay <= 0)

lg2.dot
```

#### Were delayed by at least an hour, but made up over 30 minutes in flight

```{r}
summary(flights$arr_delay)
```

negative arrival delay = made up time 

```{r}
filter(flights, dep_delay > 1 & arr_delay <= -0.5)
```


#### Departed between midnight and 6am (inclusive)

scheduled departure is easier than actual departure...
```{r}
filter(flights, hour <= 6)


```

```{r}
filter(flights, lubridate::hour(time))
```

2. `between( )`

```{r}
fm.1 <- filter(flights, month %in% c(7, 8, 9))

fm.2 <- filter(flights, between(month, 7,9))

identical(fm.1, fm.2)

```

3. 

```{r}
filter(flights, is.na(dep_time))
```

look at bottom of output table: 8255 rows

OR

```{r}
filter(flights, is.na(dep_time)) %>%
  nrow()
```

4.

```{r}
NA^0
```

anything raised to the power of zero is one (raising the power of zero is the same as dividing the number by itself)

```{r}
NA | TRUE
```

(missing OR TRUE)  is TRUE

```{r}
NA * 0
```

## 5.3.1

1.

```{r}
df <- tibble(x = c(5, 2, NA))

arrange(df, desc(is.na(x)))
```

```{r}
is.na(c(5,2,NA))
```


```{r}
summary(flights)
```

```{r}
arrange(flights, desc(is.na(dep_time)), desc(is.na(dep_delay), )
```

```{r}
sum(c(1,2,NA), na.rm = TRUE)
```




2.
most delayed flights
```{r}
arrange(flights, desc(dep_delay))
```

flights that left earliest
```{r}
arrange(flights, dep_delay)
```

3. fastest flights
```{r}
arrange(flights, air_time)
```

4. 
which flights travelled the longest

```{r}
arrange(flights, desc(air_time))
```


```{r}
filter(flights, air_time == max(flights$air_time, na.rm = TRUE))
```


## 5.4.1

1. Brainstorm as many ways as possible to select dep_time, dep_delay, arr_time, and arr_delay from flights.

```{r}
select(flights, dep_time, dep_delay, arr_time, arr_delay)
```

```{r}
select(flights, matches('^dep|^arr'))
```

select(flights, -colname1, -colname2)


```{r}
select(flights, starts_with('arr'), starts_with('dep'))
```
```{r}
colnames(flights)
```



```{r}
filter(flights, month == 1 | month == 2) %>%
  summary()
```

```{r}
filter(flights, month %in% c(1,2))
```

```{r}
1 %in% c(1,2,13)
```

```{r}
class(c(1,2,13))
```

```{r}
stringr::str_match(string = as.character(c(1,2,13)), pattern = '1')
```

```{r}
filter(.data = flights, day > month)
```


```{r}
1:10
```


```{r}
filter(flights, month == 1:2) %>%
  summary()
```



```{r}
select(flights, arr_time, arr_time)
```

```{r}

c1 <- c('arr_time', 'dep_time')

d1 <- select(flights, one_of(c1))
```








```{r}
1 == 1:2
```


```{r}
arrange(flights,day, month, year)
```



```{r}
select(flights, contains("TIME"))
```


```{r}
select(flights, contains("time"))
```

## 5.5.2

1. 

`?flights` tells us that `dep_time`, `arr_time`, `sched_dep_time` and `sched_arr_time` are times in the local timezone.

```{r}
select(flights, dep_time, sched_dep_time)
```

shows us that these variables are represented as intergers (which is an odd way to represent times...R has specific classes for date-time data which we will learn about in Chapter 16)

```{r}
unique(flights$dep_time) %>%
  sort()
```

so it looks like entries in `dep_time` that are <60 are minutes past midnight while all numbers > 100 have the last two digits as minutes and the first one or two digits as hours

to convert to minutes since midnight:

```{r}
mutate(flights, 
       dep_time_hrs  = dep_time %/% 100,   # hour component of dep_time (how many whole 100s are in dep_time)
       dep_time_mins = dep_time %% 100,    # minutes since hour  (what is the remainder after subtracting off maximum number of whole 100s that fit in dep_time)
       dep_time_msm  = dep_time_hrs*60 + dep_time_mins # dep_time minutes since midnight
) %>%
  select(dep_time, dep_time_hrs, dep_time_mins, dep_time_msm)
```


2. 

`?flights` tells us that `air_time` is the Amount of time spent in the air in minutes

```{r}
select(flights, air_time, arr_time, dep_time)
```

```{r}
mutate(flights, comparison = air_time  - (arr_time - dep_time)) %>%
  select(air_time, arr_time, dep_time, comparison)
```

the problem here is that times are represented differently in `air_time` compared to how times are represented in `arr_time` and `dep_time`

specifically `air_time` is a period of time in minutes whereas `arr_time` and `dep_time` are times in something close to hhmm format represented as integers so subtracting `dep_time` from `arr_time` will not give us the flight time in minutes

if we wanted to calculate the flight time from `arr_time` and `dep_time` the first thing to check is which flights span multiple days

`flights` contains departure dates (`year`, `month` and `day`) but no arrival dates

furthermore `?flights` tells us that `arr_time` and `dep_time` are in local time zones so we would have to put all times into a single common timezone

after doing all this we could take a similar approach to calculating the minutes since midnight above but calculate minutes since departure adding multiples of 24*60 for any days of flight time, multiples of 60 for hours of flight time and the remaining minutes of flight time

3. 

`dep_delay` is the departure delay in minutes

`dep_time` and `sched_dep_time` are the departure time and scheduled departure time in the local time zone in this hhmm as interger time format

```{r}
select(flights, dep_delay, dep_time, sched_dep_time)
```

so if `dep_time` and `sched_dep_time` are within the same clock hour then 

`dep_delay = dep_time - sched_dep_time` otherwise the differences may be out by multiples of 40 (if the delay is less than a day)

```{r}
dptms <- mutate(flights, 
       diff = dep_delay - (dep_time - sched_dep_time),
       diff.d.40 = diff/40) %>%
  select(dep_delay, dep_time, sched_dep_time, diff, diff.d.40)

dptms
```

```{r}
unique(dptms$diff.d.40) %>%
  sort()
```

```{r}
unique(unique(dptms$diff.d.40) %% 1)
```

4. 

```{r}
pull(flights, dep_delay) %>%
  sort() %>%
   tail(n = 10)
```


```{r}
x <- c(5, 1, 3, 2, 2, NA)

x
```

```{r}
min_rank(x)
```

there's a tie so `min_rank` is giving both elements that have the same rank the minimum rank and leaving a gap in the rankings (the 3rd rank) for if we want to break ties (e.g. based on the values of another variable)


We want the 10 most delayed flights and we don't know the number of unique flight times so it will be easier to ranks the delays in descending order and select ranks 1:10

we can rank in descending order as follows:
```{r}
min_rank(desc(x))
```

```{r}
x
```


```{r}
mutate(flights, 
       delay.rank = min_rank(desc(arr_delay))
       ) %>%
  filter(delay.rank < 11) %>%
    select(flight, arr_delay, delay.rank) %>%
      arrange(delay.rank)
```

no ties here so we don't have to worry about breaking ties

5. 

```{r}
x1 <- 1:3 + 1:10

x1
```

R recyles the shorter vector when you try to use two vectors of the different lengths

so what we're actually getting here is

```{r}

x2 <- c(1:3,1:3,1:3,1) + 1:10

x1 - x2
```

6.
see `?Trig`

## 5.6.7

1.  

Different ways to assess typical delay characteristics of a group of flights:

min(), max(), IQR(), sd(), IQR() & mad()

of departure / arrival delays

median arrival and departure delays

e.g. if we group flights by carrier

```{r}
group_by(flights, carrier) %>%
  summarise(dep_del_med = median(dep_delay, na.rm = TRUE),
            arr_del_med = median(arr_delay, na.rm = TRUE)) %>%
    arrange(dep_del_med, arr_del_med)
```

minimum arrival delay

```{r}
group_by(flights, carrier) %>%
  summarise(min.arr.del = min(arr_delay, na.rm = TRUE)) %>%
    arrange(desc(min.arr.del))

```

counts of the numbers of flights in each group with particular characteristics

A flight is 15 minutes early 50% of the time, and 15 minutes late 50% of the time.

```{r}
group_by(flights, carrier) %>%
  summarise(prct.15.early = sum(arr_delay[arr_delay == -15], na.rm = TRUE)/n(), 
            prct.15.late  = sum(dep_delay[dep_delay == 15], na.rm = TRUE)/n()
           ) %>%
    filter(prct.15.early == 0.5 & prct.15.late == 0.5)
```

A flight is always 10 minutes late.

```{r}
group_by(flights, carrier) %>%
  filter(arr_delay >= 10 | dep_delay >= 10)
  summarise(prct.15.early = sum(arr_delay[arr_delay == -15], na.rm = TRUE)/n(), 
            prct.15.late  = sum(dep_delay[dep_delay == 15], na.rm = TRUE)/n()
           ) %>%
    filter(prct.15.early == 0.5 & prct.15.late == 0.5)
```


2.

```{r}
not_cancelled <- flights %>% 
  filter(!is.na(dep_delay), !is.na(arr_delay))
```

alternative to 

```{r}
out1.tb <- not_cancelled %>% count(dest)

out1.tb
```

```{r}
out2.tb <- group_by(not_cancelled, dest) %>%
  summarise(n = n())

out2.tb
```

```{r}
identical(out1.tb, out2.tb)
```

alternative that doesn't use `count()` to

```{r}
out3.tb <- not_cancelled %>% 
  count(tailnum, wt = distance)

out3.tb
```

```{r}
out4.tb <- group_by(not_cancelled, tailnum) %>%
  summarise(n = sum(distance))

out4.tb
```

```{r}
identical(out3.tb, out4.tb)
```

3. the actual departure time and arrival time if we have a record of one ore more of these the flight seems likely to have occured

4. 

Look at the number of cancelled flights per day. Is there a pattern?

```{r}
filter(flights, is.na(dep_time) & is.na(arr_time)) %>%
  group_by(year, month, day) %>%
    summarise(n.canceled = n()) %>%
      ggplot(aes(x = n.canceled)) +
        geom_histogram() + 
          labs(x = 'Daily Flight Cancelations')
```

Is the proportion of cancelled flights related to the average delay?

```{r}
mutate(flights,
       canceled = (is.na(arr_time) | is.na(dep_time))) %>%
  group_by(year, month, day) %>%
    summarise(mean.dep.delay = mean(dep_delay, na.rm = TRUE),
              prop.canceled = sum(canceled)/n()) %>%
      ggplot(aes(x = mean.dep.delay, y = prop.canceled)) +
        geom_point(alpha = 0.25) + 
          stat_smooth() + 
            theme_bw()
```

5. 

Which carrier has the worst delays? 

How do you define worst delays? There are many aspects of the distributions of delays that could be used for ranking carriers... 
```{r}
ggplot(flights, aes(x = carrier, y = arr_delay)) + 
  geom_boxplot()
```

or if you prefer...

```{r, fig.height=8}
ggplot(flights, aes(x = arr_delay)) + 
  geom_density() + 
    facet_grid(carrier ~ .)
```

(see the package [`ggjoy`](https://cran.r-project.org/web/packages/ggjoy/vignettes/introduction.html) to make joy plots)

```{r, fig.height=8}
ggplot(flights, aes(x = arr_delay)) + 
  geom_density() + 
    facet_grid(carrier ~ .) + 
     xlim(-75, 250)
```


e.g. mean delays greater than zero minutes

```{r}
group_by(flights, carrier) %>%
  filter(arr_delay > 0) %>%
    summarise(mean.arr.del = mean(arr_delay, na.rm = TRUE)) %>%
     arrange(desc(mean.arr.del))
```

Challenge: can you disentangle the effects of bad airports vs. bad carriers? Why/why not? (Hint: think about flights %>% group_by(carrier, dest) %>% summarise(n()))

```{r}
group_by(flights, carrier, dest) %>%
  count() %>%
    arrange(n)
```

some carriers only have one flight to a destination

without replication within carrier:destination combinations we won't be able to estimate the effects of such combinations in a model...

6. 

`count(sort = TRUE)`


```{r}
group_by(flights, carrier, dest) %>%
  count()
```

```{r}
group_by(flights, carrier, dest) %>%
  count(sort = TRUE)
```

```{r}
group_by(flights, carrier, dest) %>%
  summarise(n = n()) %>%
    arrange(desc(n))
```

It is worth reading about Window Functions:
`vignette("window-functions")`

## 5.7.1

2. 

How do we define the worst on-time record?

Here I find the highest proportion of flights with an arrival delay greater than zero:

```{r}
group_by(flights, tailnum) %>%
  summarise(prop.late = sum(arr_delay > 0, na.rm = TRUE)/n())
    arrange(desc(prop.late))
```

3.

Ignoring the possible effects of origin and destination airport identity (and excluding fights that were canceled)
Finding the hour (of the day) with the lowests mean arrival delay
```{r}

group_by(flights, hour) %>%
  summarise(mean_arr_del = mean(arr_delay, na.rm = TRUE)) %>%
    arrange(mean_arr_del)


```

4. 

Destinations are only half the story when it comes to delays but to answer the question (again ignoring flights that we canceled)

```{r}
mutate(flights, total.delay = arr_delay + dep_delay) %>%
  group_by(dest) %>%
    summarise(dest.total.delay = sum(total.delay, na.rm = TRUE)) %>%
      arrange(desc(dest.total.delay))
```

we haven't learned about joins yets (these come in Chapter 13) but we can use `filter` to look up the full names of airports from their FAA code:
```{r}
filter(airports, faa %in% c('ATL', 'ORD', 'FLL'))
```

For each, flight, compute the proportion of the total delay for its destination.

```{r}
mutate(flights, total.delay = arr_delay + dep_delay) %>%
  group_by(dest) %>%
    mutate(fpodtd = total.delay/sum(total.delay, na.rm = TRUE)) %>% # flight proportion of destination total delay
      arrange(desc(fpodtd)) %>%
        select(flight, carrier, dest, total.delay, fpodtd)
```

Proportions should be <= 1 

What is happening here is negative delays are canceling out positive delays when I calculate `total.delay`

We could look at only the flights with positive arrival delays (i.e. only the flights that were late to arrive at their destination) and restrict ourselves to examining arrival delays alone
```{r}
filter(flights, arr_delay > 0) %>%
  group_by(dest) %>%
    mutate(fpodtad = arr_delay/sum(arr_delay, na.rm = TRUE)) %>% # flight proportion of destination total arrival delay
      arrange(desc(fpodtad)) %>%
        select(flight, carrier, dest, arr_delay, fpodtad)
```

5. 

Delays are typically temporally correlated: even once the problem that caused the initial delay has been resolved, later flights are delayed to allow earlier flights to leave. Using lag() explore how the delay of a flight is related to the delay of the immediately preceding flight.


```{r}
group_by(flights, origin) %>%
  count()
```

```{r}
group_by(flights, origin) %>%
  arrange(year, month, day, hour) %>%
    mutate(dep_delay_lag_1 = lag(x = dep_delay, n = 1)) %>%
      ungroup() %>%
        ggplot(aes(x = dep_delay, y = dep_delay_lag_1)) +
            geom_point(alpha = 0.2) +
              facet_grid(origin ~ .) + 
                geom_smooth()
```

6. Look at each destination. Can you find flights that are suspiciously fast? (i.e. flights that represent a potential data entry error). Compute the air time a flight relative to the shortest flight to that destination. Which flights were most delayed in the air?


Recall that these data are all for flights departing New York City

```{r}
unique(flights$origin)
```

What is a suspiciously short air time? 

Counting the number of flights with an air time < 1st quartile of air times to their respective destinations

```{r}
group_by(flights, dest) %>%
  filter(air_time < quantile(air_time, 0.25, na.rm = TRUE)) %>%
    count(sort = TRUE)
```

Counting the number of flights with an air time < 5th percentile of air times to their respective destinations

```{r}
group_by(flights, dest) %>%
  filter(air_time < quantile(air_time, 0.05, na.rm = TRUE)) %>%
    count(sort = TRUE)
```

so yes there are some suspiciously fast flights

Compute the air time a flight relative to the shortest flight to that destination. 

```{r}
group_by(flights, dest) %>%
  mutate(add.air.time = air_time - min(air_time)) %>% # air time beyond the minimum air time to destination
    ungroup() %>%
      ggplot(aes(x = add.air.time)) +
        geom_histogram()
```

Which flights were most delayed in the air?

```{r}
group_by(flights, dest) %>%
  mutate(add.air.time = air_time - min(air_time)) %>% # air time beyond the minimum air time to destination
    ungroup() %>%
      arrange(desc(add.air.time)) %>%
        select(flight, carrier, dest, add.air.time)
```

7. Find all destinations that are flown by at least two carriers. Use that information to rank the carriers.

Here is a very simple ranking of carriers, a lower mean rank is better but it is also better to have a lower mean rank over many different routes rather thank just one (or at least I think it is).

There's is probably a better way to rank these carriers than what I have done below (ranking systems are a whole field of their own)

```{r}
group_by(flights, dest) %>%
  mutate(carriers.to.dest = n_distinct(carrier)) %>%
    filter(carriers.to.dest > 1) %>%
      ungroup() %>%
        group_by(dest, carrier) %>%
          summarise(mean.air.time = mean(air_time, na.rm = TRUE)) %>%
            ungroup() %>%
              group_by(dest) %>%
                mutate(carrier.dest.rank = rank(mean.air.time)) %>%
                  ungroup() %>%
                    group_by(carrier) %>% 
                      summarise(carrier.mean.rank = mean(carrier.dest.rank),
                                carrier.n.dest = n_distinct(dest)
                                ) %>%
                        arrange(carrier.mean.rank)
 
```


8. For each plane, count the number of flights before the first delay of greater than 1 hour.

Planes are uniquely identified by tail numbers

number of observations until a condition is met

see 

`vignette("window-functions")`

`cumall` is the one we're after

it's a cumulative version of `all()` which means that it cummulatively tests whether the expression is true for all rows up to and including the current row e.g. 

```{r}
tibble(x = c(1, 2, 3, 1, 2, 2),
       cumall(x < 3))
```

```{r}
group_by(flights, tailnum) %>%
  arrange(year, month, day, hour) %>%
    mutate(cum.dd = cumall(dep_delay <= 1)) %>%
      summarise(nf = sum(cum.dd)) %>%  # number of flights until first delay greater than 1 hour
        arrange(desc(nf))
```
